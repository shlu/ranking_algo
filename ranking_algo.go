//package Ranking_Algo
package main

import (
	
	"encoding/csv"

	"github.com/gin-gonic/gin"
	"os/exec"

	"io"
	"log"

	"bytes"
	"fmt"
	"github.com/mash/gokmeans"
	//"github.com/recursionpharma/go-csv-map"
	"gonum.org/v1/gonum/stat"
	"os"
	"strings"
	//"github.com/knightjdr/hclust"
	"strconv"
)



type Reader struct {
	Reader  *csv.Reader
	Columns []string
}

type convert_float func(map[string]string, float64) float64;

func get_id(row map[string]string) int{
	var x  = row["ssid_id"]
	i, _ := strconv.Atoi(x)
	return i
}


func update_numeric_description(row map[string]string, description float64) float64{
	if strings.Contains(row["description"], "no internet access") {
		return description-2
	} else if strings.Contains(row["description"], "drop at random times"){
		return description - 1
	} else {
		return description
	}
}

func update_numeric_internet_speed (row map[string]string, internet_speed float64) float64 {
	if strings.Contains(row["description"], "very slow"){
		return internet_speed - 1
	} else {
		return internet_speed
	}
}


func update_numeric_group (row map[string]string, group float64) float64 {
	if strings.Contains(row["group"], "chur"){
		return 3
	} else if strings.Contains(row["group"], "community"){
		return 2
	} else if strings.Contains(row["group"], "wholesale"){
		return 1
	} else{
		return 2
	}
}

func update_network_speed(row map[string]string, network_speed float64) float64 {
	if strings.Compare(row["network_speed"], "")==0 {
		return network_speed
	} else{
		f, _ := strconv.ParseFloat(row["network_speed"], 64)
		return network_speed + f
	}
}

func update_connection (row map[string]string, connection float64) float64{
	return connection+1
}

func update_duration (row map[string]string, duration float64) float64 {
	if strings.Compare(row["duration"], "")==0 {
		return 0
	} else{
		f, _ := strconv.ParseFloat(row["duration"], 64)
		return f + duration
	}
}

func update_status (row map[string]string, status float64) float64{
	if strings.Compare(row["status"], "") == 0{
		return 0
	} else{
		f, _ := strconv.ParseFloat(row["status"], 64)
		return f + status
	}
}


func standardise_values(input_array []float64) []float64 {
	var standardise_array = []float64{}
	std := stat.StdDev(input_array, nil)
	mean := stat.Mean(input_array, nil)

	for _, x := range input_array{
		result := (x-mean)/std
		standardise_array = append(standardise_array, result)
	}

	return standardise_array

}

func normalise_values(input_array []float64) []float64{
	var normalised_array = []float64{}
	//mean := stat.Mean(input_array, nil)
	max := input_array[0]
	min := input_array[0]
	for _, v := range input_array{
		if v > max{
			max = v
		}
		if v < min{
			min = v
		}
	}
	for _, x := range input_array{
		result := (x-min)/(max-min)
		normalised_array = append(normalised_array, result)
	}
	return normalised_array

}

func dict_to_matrix(a_dict map[string]map[string]float64, headings []string)[][]string{
	var matrix = [][]string{}
	for identifier, values := range a_dict{
		//var values = []int{};
		var v  = []string{}
		v = append(v, identifier)
		for _, h := range headings{
			v = append(v, fmt.Sprintf("%f", values[h]))
		}
		matrix = append(matrix, v)
	}
	return matrix
}


func CSVToMap(reader io.Reader) []map[string]string {
	r := csv.NewReader(reader)
	rows := []map[string]string{}
	var header []string
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if header == nil {
			header = record
		} else {
			dict := map[string]string{}
			for i := range header {
				if strings.Compare(record[i], "") != 0 {
					dict[header[i]] = record[i]
				}

			}
			rows = append(rows, dict)
		}
	}
	return rows
}

func csv_to_dict_float(filename string, identifier string, headings []string, functions map[string]convert_float) map[string]map[string]float64{
	var wifi_networks =  map[string]map[string]float64{}

	f, err := os.Open(filename)
	if err != nil{
		log.Fatalln("Could not open the csvfile", err)
	}
	csvMap := CSVToMap(f)
	for _, row := range csvMap{

		var hm = map[string]float64{}
		for _, h := range headings{
			hm[h] = 0.0
		}
		var cur_wifi_info = map[string]float64{}
		if _, ok := wifi_networks[row[identifier]]; ok {
			//do something here
			 cur_wifi_info = wifi_networks[row[identifier]]
		} else {
			wifi_networks[row[identifier]] = hm
			cur_wifi_info = wifi_networks[row[identifier]]
		}
		for k_cur, _ := range cur_wifi_info{
			fn := functions[k_cur]
			cur_wifi_info[k_cur] = fn(row, cur_wifi_info[k_cur])
		}
		wifi_networks[row[identifier]] = cur_wifi_info
	}

	return wifi_networks
}

func dict_to_csv(filename string, dict map[string]map[string]string, headings []string){
	var rows = [][]string{}
	//rows = append(rows, []string{identifier})
	rows = append(rows, headings)
	headings = append(headings[:0], headings[0+1:]...)
	for k, mp := range dict{
		r := []string{}
		r = append(r, k)
		for _, h := range headings{
			//fmt.Println(h)
			//fmt.Println(mp[h])
			r = append(r, mp[h])
		}
		rows = append(rows, r)
	}
	csvfile, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create file ", err)
	}
	csvwriter := csv.NewWriter(csvfile)
	for _, row := range rows {
		_ = csvwriter.Write(row)
	}

	csvwriter.Flush()
	csvfile.Close()
}


func storeName(csvMap []map[string]string, headings []string) map[string]string {
	var ret = map[string]string{}
	for _, row := range csvMap{
		ret[row["id"]]  = row["name"]
	}
	return ret
}


func run_k_means(data [][]float64, n_clusters int) []int{
	//fmt.Print("data")
	//fmt.Print(data)
	observations := []gokmeans.Node{}
	for _, r := range data{
		observations = append(observations, gokmeans.Node(r))
	}
	var labels = []int{}
	if success, centroids := gokmeans.Train(observations, n_clusters, 100); success {
		// get clusters
		for _, observation := range observations {
			index := gokmeans.Nearest(observation, centroids)
			labels = append(labels, index)
		}
	}
	return labels
}


func get_lable_mean(data []map[string]float64, num int, headings []string) map[string]float64{
	avg_h := map[string]float64{}
	for _, h := range headings{
		avg_h[h] = 0.0
	}
	for _, mp := range data{
		for _, h := range headings{
			avg_h[h] += mp[h]
		}
	}
	for _, h := range headings{
		avg_h[h] = avg_h[h]/float64(num)
	}
	return avg_h
}


func zip(lists ...[]int) func() []int {
	zip := make([]int, len(lists))
	i := 0
	return func() []int {
		for j := range lists {
			if i >= len(lists[j]) {
				return nil
			}
			zip[j] = lists[j][i]
		}
		i++
		return zip
	}
}

func transpose(slice [][]float64) [][]float64 {
	xl := len(slice[0])
	yl := len(slice)
	result := make([][]float64, xl)
	for i := range result {
		result[i] = make([]float64, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = slice[j][i]
		}
	}
	return result
}

func FloatToString(input_num float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(input_num, 'f', 6, 64)
}

func main() {
	//conn to bd
	r := gin.Default()
	r.GET("get_wifi_data")
	cmd := exec.Command("python3", "get_wifi_data.py")
	_, err := cmd.Output()
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	if err != nil {
		println(fmt.Sprint(err) + ": " + stderr.String())
	// if false{
	} else{
		//create dict
		identifier := "ssid_id"
		headings := []string{"description", "internet_speed", "group"}
		var numeric_functions = map[string] convert_float{}
		numeric_functions["description"] = update_numeric_description
		numeric_functions["group"] = update_numeric_group
		numeric_functions["internet_speed"] = update_numeric_internet_speed
		numeric_functions["network_speed"] = update_network_speed

		wifi_dict := csv_to_dict_float("rankings_data.csv", identifier, headings, numeric_functions)

		// Append columns to dict to include duration and connection information
		headings_appended_1 := []string{"duration", "connections"}
		numeric_function := map[string] convert_float{"duration" : update_duration, "connections" : update_connection}
		wifi_dict_appended_1 := csv_to_dict_float("ssids_connections.csv", identifier, headings_appended_1, numeric_function)

		// append status information of wifi
		headings_appended_2 := []string{"status", "conn_wifi"}
		funct := map[string] convert_float{"status": update_status, "conn_wifi": update_connection}
		wifi_dict_appended_2 := csv_to_dict_float("wifi_status.csv", identifier, headings_appended_2, funct)


		// Convert total duration to average duration
		for _, v := range wifi_dict_appended_1{
			v["duration"] = v["duration"]/v["connections"]
		}

		for _, v := range wifi_dict_appended_2{
			v["status"] = v["status"]/v["conn_wifi"]
		}

		// var conns_s = map[string]map[string]string{}
		// for k, v := range wifi_dict_appended_2{
		// 	nmp := map[string]string{"status": FloatToString(v["status"])}
		// 	conns_s[k] = nmp
		// }

		// fmt.Println(wifi_dict_appended_2)

		// header_cd := []string{"ssid_id", "status"}
		// dict_to_csv("conns_status.csv", conns_s, header_cd)

		// Merge two dicts
		for k, v := range wifi_dict{
			v1, ok := wifi_dict_appended_1[k]
			if ok {
				for v1_k, v1_v := range v1{
					wifi_dict[k][v1_k] = v1_v
				}
			} else {
				for _, h := range headings_appended_1{
					v[h] = 0
				}
			}
		}

		for k, v := range wifi_dict{
			v1, ok := wifi_dict_appended_2[k]
			if ok {
				for v1_k, v1_v := range v1{
					wifi_dict[k][v1_k] = v1_v
				}
			} else {
				for _, h := range headings_appended_2{
					v[h] = 0
				}
			}
		}


		for _, h := range headings_appended_1{
			headings = append(headings, h)
		}

		for _, h := range headings_appended_2{
			headings = append(headings, h)
		}

		// convert dict to matrix
		matrix := dict_to_matrix(wifi_dict, headings)
		//fmt.Print(matrix)

		ssid_ids := make([]string, 0)
		for _, row := range matrix {
			ssid_ids = append(ssid_ids, row[0])
		}

		// standardise matrix of values, no ssids
		standardised_matrix := make([][]float64, 0)

		for i := 1; i < len(matrix[0]); i++ {
			input_array := make([]float64, 0)
			for _, row := range matrix{
				s, _ := strconv.ParseFloat(row[i], 64)
				input_array = append(input_array, s)
			}
			standardised_array := standardise_values(input_array)
			standardised_matrix = append(standardised_matrix, standardised_array)
		}
		standardised_matrix = transpose(standardised_matrix)
		k_means_labels := run_k_means(standardised_matrix, 4)
		fmt.Print(k_means_labels)
		var final_rankings = map[string]int{}
		var ssid_ids_int = []int{}
		for _, v := range ssid_ids{
			i, _ := strconv.Atoi(v)
			ssid_ids_int = append(ssid_ids_int, i)
		}
		iter := zip(ssid_ids_int, k_means_labels)
		for tuple := iter(); tuple != nil; tuple = iter() {
			final_rankings[strconv.Itoa(tuple[0])] = tuple[1]
		}


		f, err := os.Open("ssid_info.csv")
		if err != nil{
			log.Fatalln("Could not open the csvfile", err)
		}
		csvMap := CSVToMap(f)
		name_info_headings := []string{"id", "name"}
		names := storeName(csvMap, name_info_headings)

		// combine names into labels csv
		var combined_info = map[string]map[string]string{}
		for k, v := range final_rankings{
			var mp = map[string]string{}
			v1, ok := names[k]
			mp["label"] = strconv.Itoa(v)
			if ok {
				mp["name"] = v1
			}
			combined_info[k] = mp
		}

		label0_n := 0
		label1_n := 0
		label2_n := 0
		label3_n := 0

		var l0_info = []map[string]float64{}
		var l1_info = []map[string]float64{}
		var l2_info = []map[string]float64{}
		var l3_info = []map[string]float64{}


		for ssid, mp := range wifi_dict{
			label := combined_info[ssid]["label"]
			var mp_app = map[string]float64{}
			if strings.Compare(label, "0") == 0{
				label0_n += 1
				for _, h := range headings{
					mp_app[h] = mp[h]
				}
				l0_info = append(l0_info, mp_app)
			}

			if strings.Compare(label, "0") == 0{
				label0_n += 1
				for _, h := range headings{
					mp_app[h] = mp[h]
				}
				l0_info = append(l0_info, mp_app)
			}

			if strings.Compare(label, "1") == 0{
				label1_n += 1
				for _, h := range headings{
					mp_app[h] = mp[h]
				}
				l1_info = append(l1_info, mp_app)
			}

			if strings.Compare(label, "2") == 0{
				label2_n += 1
				for _, h := range headings{
					mp_app[h] = mp[h]
				}
				l2_info = append(l2_info, mp_app)
			}

			if strings.Compare(label, "3") == 0{
				label3_n += 1
				for _, h := range headings{
					mp_app[h] = mp[h]
				}
				l3_info = append(l3_info, mp_app)
			}
		}

		//l0_mean_info := get_lable_mean(l0_info, label0_n, headings)
		//l1_mean_info := get_lable_mean(l1_info, label1_n, headings)
		//l2_mean_info := get_lable_mean(l2_info, label2_n, headings)
		//l3_mean_info := get_lable_mean(l3_info, label3_n, headings)

		//fmt.Println("label0: ")
		//fmt.Println(l0_mean_info)
		//fmt.Println("label1: ")
		//fmt.Println(l1_mean_info)
		//fmt.Println("labrl2:" )
		//fmt.Println(l2_mean_info)
		//fmt.Println("label3: ")
		//fmt.Println(l3_mean_info)

		//fmt.Print(final_rankings)
		header := []string{"ssid_id", "name", "label"}
		dict_to_csv("final_rankings.csv", combined_info, header)

		//fmt.Println(wifi_dict["11555"])
		//fmt.Println(wifi_dict["18949"])
		//fmt.Println(wifi_dict["447"])
		//fmt.Println(wifi_dict["18947"])
		//fmt.Println(wifi_dict["88"])

	}


}