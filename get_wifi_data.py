import sys
import csv
import psycopg2
import numpy as np
import pandas as pd
from datetime import datetime


def write_to_csv(fileName, colName, data):
    with open(fileName, 'w') as out:
        csv_out = csv.writer(out)
        csv_out.writerow(colName)
        for row in data:
            csv_out.writerow(row)


def generate_telco_report (sqlcmd, database = 'churmasternode', user='churoot', password='XvX&hJScVSdZ$3', host='churiot.com', port=10001):
    connection = psycopg2.connect(database=database, user=user, password=password, host=host, port=port)
    cursor = connection.cursor()

    # start_timestamp = str(start)
    # end_timestamp = str(end)

    cursor.execute(sqlcmd)

    info = cursor.fetchall()

    return info

if __name__ == "__main__":
    # start = sys.argv[1]
    # end = sys.argv[2]

    # get status info:
    sqlcmd = "select ssid_id, status from wifi_health_informations where ssid_id is not null;"
    status_info = generate_telco_report(sqlcmd)
    write_to_csv("wifi_status.csv", ['ssid_id', 'status'], status_info)

    # ranking_data.csv
    sqlcmd_rd = """select ssid_id, description,ssids.group, network_speed
                    from ssids_reports, ssids
                    where ssids_reports.ssid_id = ssids.id;"""
    ranking_dt = generate_telco_report(sqlcmd_rd)
    write_to_csv("rankings_data.csv", ['ssid_id', 'description', 'group', 'network_speed'], ranking_dt)

    # connections
    sql_conn = """select * from
                ssids_connections;"""
    connection_info = generate_telco_report(sql_conn)
    write_to_csv("ssids_connections.csv", ['id', 'user_id', 'ssid_id', 'state', 'duration', 'created_at', 'updated_at'], connection_info)

    # info
    sql_ssid_info = "select id, ssids.name from ssids;"
    name_info = generate_telco_report(sql_ssid_info)
    write_to_csv("ssid_info", ["id", "name"], name_info)

    sys.stdout.write("success")
